/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  Image,
  View
} from 'react-native';

class AwesomeProject extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
            style={styles.logo}
            source={require('./fr_logo_small.png')}
        />
        <Text style={styles.welcome}>
          Welcome Rivallers!
        </Text>
        <Text style={styles.instructions}>
          Are you ready to play the game..
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
                                 logo: {
                                 
                                 },
});

AppRegistry.registerComponent('AwesomeProject', () => AwesomeProject);
